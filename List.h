//List.h
#include <stdio.h> //                          
#include <stdlib.h> //                     

//Структура узла списка
typedef struct Node {
    int value;
    struct Node* next;
    struct Node* prev;
} Node;

//Структура целого списка
typedef struct DblLinkedList {
    size_t size;
    Node* head;
    Node* tail;
} DblLinkedList;

//Структура для передачи нескольких аргументов в функцию потока
typedef struct thread_data {
    Node* node;
    int id; //переменная для идентификации переданного в поток узла (head / tail)
} thread_data;

//////////Прототипы функций/////////////
//Функция создания корня списка                               
DblLinkedList* createDblLinkedList(void);

//Функция добавления элемента в конец списка (заполнение)                                                     
void* pushBack(void* list, int value);

//Функция вывода списка на экран                         
void* printDblLinkedList(void* list);

//Фнукция для запуска в потоке (подсчет количества битов в узлах списка)                
void* count_bits(void* data);
