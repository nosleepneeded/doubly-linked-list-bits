//List.c
#include "List.h"
#include <pthread.h>
extern pthread_mutex_t mutex;
extern int flag; //Для обработки последнего элемента

//Функция создания корня списка 
DblLinkedList* createDblLinkedList(void) {
    DblLinkedList* tmp = (DblLinkedList*)malloc(sizeof(DblLinkedList));
    tmp->size = 0;
    tmp->head = tmp->tail = NULL;
    return tmp;
}
//Функция добавления элемента в конец списка (заполнение)
void* pushBack(void* main_list, int value) {
    DblLinkedList* list = (DblLinkedList* )main_list;
    Node* tmp = (Node*)malloc(sizeof(Node));
    if (tmp == NULL) {
        exit(3);
    }
    tmp->value = value;
    tmp->next = NULL;
    tmp->prev = list->tail;
    if (list->tail) {
        list->tail->next = tmp;
    }
    list->tail = tmp;

    if (list->head == NULL) {
        list->head = tmp;
    }
    list->size++;
}
//Функция вывода списка на экран
void* printDblLinkedList(void* main_list) {
    DblLinkedList* list = (DblLinkedList* )main_list;
    Node* tmp = list->head;
    printf("Random values of list: ");
    while (tmp != NULL) {
        printf("%d ", tmp->value);
        tmp = tmp->next;
    }
    printf("\n");
}
//Фнукция для подсчета количества битов в узлах списка
void* count_bits(void* dataas) {
    thread_data* data = (thread_data*)dataas;
    Node* tmp = data->node; //обрабатываемый элемент
    Node* del = NULL;   //удаляемый элемент
    int id = data->id; 
    int bits = 0;  //для подсчета единичных или нулевых битов в узлах
    int total_bits = 0;  //общее количество единичных или нудевых битов
    int counter = 0;  //счетчик обработанных потоком элементов
    do {
        pthread_mutex_lock(&mutex);
        if (flag == 0) {     //флаг = 0 если элемент не последний, либо последний, но еще не был обработан
            if (id) { //если id = 1, значит передан head
                bits = 0;
                printf("\nNumber %d", tmp->value);
                while (tmp->value) {
                    if (tmp->value % 2 == 0) {
                        bits++;
                    }
                    tmp->value >>= 1;
                }
                printf(" has '0' bits: %d", bits);
                total_bits += bits;
                counter++;
                del = tmp;  //ставим указатель на удаляемый элемент
                tmp = tmp->next; //переходим к следующему
                if (tmp != NULL) {  //если del не последний, тогда просто очищаем его
                    tmp->prev = NULL;
                    free(del);
                    pthread_mutex_unlock(&mutex);
                }
                else {  //если del последний, тогда очищаем его и ставим flag = 1
                    free(del);
                    flag = 1;
                    pthread_mutex_unlock(&mutex);
                }
            }
            else if (!id) { //если id = 0, значит передан tail
                bits = 0;
                printf("\nNumber %d", tmp->value);
                while (tmp->value) {
                    if (tmp->value % 2 == 1) {
                        bits++;
                    }
                    tmp->value >>= 1;
                }
                printf(" has '1' bits: %d", bits);
                total_bits += bits;
                counter++;
                del = tmp;  //ставим указатель на удаляемый элемент
                tmp = tmp->prev; //переходим к предыдущему
                if (tmp != NULL) { //если del не последний, тогда просто очищаем его
                    tmp->next = NULL;
                    free(del);
                    pthread_mutex_unlock(&mutex);
                }
                else {  //если del последний, тогда очищаем его и ставим flag = 1
                    free(del);
                    flag = 1;
                    pthread_mutex_unlock(&mutex);
                }
            }
        }
        else { //если флаг = 1, значит последний элемент уже был учтен другим потоком, выходим из цикла
            pthread_mutex_unlock(&mutex);
            break;
        }
        printf("\ntotal %s bits: %d\n", //Общее количество битов 
            id ? "0" : "1",
            total_bits);

    } while (tmp != NULL);
    pthread_mutex_lock(&mutex);
    printf("\ntotal number of proccessed nodes by thread#%s: %d", //Подсчет пройденных элементов
        id ? "0" : "1",
        counter);
    printf("\n");
    pthread_mutex_unlock(&mutex);
}
