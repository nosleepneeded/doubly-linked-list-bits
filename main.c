//main.c
#define _CRT_SECURE_NO_WARNINGS 
#include <time.h>
#include <pthread.h>
#include "List.h"
int flag = 0; //Для обработки последнего элемента
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

int main() {
    srand(time(NULL));
    DblLinkedList* main_list = createDblLinkedList();
    thread_data* tmp_head = (thread_data*)malloc(sizeof(thread_data)); //Выделяем память под структуры
    thread_data* tmp_tail = (thread_data*)malloc(sizeof(thread_data)); //для передачи аргументов в поток
    
    if (main_list == NULL) { //Если список не создан
        exit(EXIT_FAILURE);
    }
    int size = 0; 
    int i = 0;
    int value = 0;
    int id = 1;

    printf("Enter size: ");
    scanf("%d", &size);
    fflush(stdin);

    if (size < 1) {
        printf("%s", (size < 0) ? "Список не может состоять из отрицательного кол-ва  элементов" : "Список пуст" );
    }
    else {
        while (i < size) {              //
            value = rand() % 100 + 1;   //Заполняем список рандомными числами
            pushBack(main_list, value); //
            i++;
        }
        printDblLinkedList(main_list); //Выводим список на экран

        tmp_head->node = main_list->head; //Передаем head в первую структуру
        tmp_tail->node = main_list->tail; //Передаем tail в первую структуру

        tmp_head->id = id; //Передаем идентифиактор переданного в поток узла (1 = head)
        tmp_tail->id = !id; //(0 = tail)

        //Создание потоков, обработка списка
        pthread_t thread1, thread2;
        pthread_create(&thread1, NULL, count_bits, tmp_head);
        pthread_create(&thread2, NULL, count_bits, tmp_tail);
        pthread_join(thread1, NULL);
        pthread_join(thread2, NULL);
    }
    //Очищаем память, выделенную под структуры, удаляем мьютекс
    free(tmp_head);
    free(tmp_tail);
    pthread_mutex_destroy(&mutex); 
    return 0;
}

